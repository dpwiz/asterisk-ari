{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Asterisk.ARI
    ( envOptions
    , connect
    , wsAuto, wsDisabled
    , waitReady
    , handleEvents, allEvents, ignoreEvents
    , invoke, invokeStatus
    , logDebug, logDebugShow, logDebugText
    , module Asterisk.ARI.Types
    ) where

import           Control.Concurrent               (threadDelay)
import qualified Control.Concurrent.Chan.Unagi    as CH
import           Control.Exception                (try, throwIO)
import           Control.Monad                    (forever)
import           Data.Aeson                       (eitherDecode')
import qualified Data.ByteString.Char8            as BS
import qualified Data.ByteString.Lazy.Char8       as BSL
import           Data.Maybe                       (fromMaybe)
import qualified Data.Text                        as T
import           Data.Typeable
import qualified Network.HTTP.Client              as HTTP
import qualified Network.HTTP.Client.TLS          as HTTP
import qualified Network.HTTP.Types.Status        as HTTP
import qualified System.Log.FastLogger            as L
import           System.Environment               (lookupEnv)

import           Asterisk.ARI.Transport.REST
import           Asterisk.ARI.Transport.WebSocket
import           Asterisk.ARI.Types
import qualified Asterisk.ARI.Applications as Apps

envOptions :: IO Options
envOptions = Options
    <$> env "ARI_APP"      "ari-app"           id
    <*> env "ARI_KEY"      "asterisk:asterisk" pair
    <*> env "ARI_INSECURE" ""                  null
    <*> env "ARI_HOST"     "localhost"         id
    <*> env "ARI_PORT"     "8088"              read
    <*> env "ARI_PATH"     "/"                 id
    <*> env "ARI_DEBUG"    ""                  split
  where
    env :: String -> String -> (String -> a) -> IO a
    env key def f = fmap
        (f . fromMaybe def)
        (lookupEnv key)

    pair = fmap (drop 1)
         . break (== ':')

    split s =
        case dropWhile (== ',') s of
            "" -> []
            s' ->
                let (w, s'') = break (== ',') s'
                in  w : split s''

-- | Automatically initiate a WS event listener
-- and reconnect it after 500ms no matter what.
wsAuto :: WSOptions
wsAuto = WSOptions
    { wsConnect = True
    , wsOnConnect = const $ pure ()
    , wsFinally = const $ do
        threadDelay 500000
        pure True
    }

-- | Do not initiate a WS connection (thus don't register an app)
wsDisabled :: WSOptions
wsDisabled = WSOptions
    { wsConnect = False
    , wsOnConnect = const $ pure ()
    , wsFinally = const $ pure False
    }

connect :: Options -> IO ARI
connect = connectWith wsAuto

connectWith :: WSOptions -> Options -> IO ARI
connectWith ws opts = ARI
    <$> pure opts
    <*> L.newStdoutLoggerSet L.defaultBufSize
    <*> HTTP.newManager HTTP.tlsManagerSettings
    <*> connectEvents opts ws

waitReady :: ARI -> IO (Either Int ARI)
waitReady ari = go >>= check
    where
        myName = T.pack . ariApp $ ariOptions ari
        go = threadDelay 500000 >> invokeStatus ari (Apps.Get myName)
        check st = case st of
            Left 404 -> go >>= check
            Left code -> pure $ Left code
            Right _ -> pure $ Right ari

handleEvents :: ARI -> EventFilter -> EventHandler -> IO ()
handleEvents ari shouldHandle handler = subscribe >>= handle
  where
    subscribe = CH.dupChan . fst $ ariEventsApp ari
    handle ch = forever $ do
        event <- CH.readChan ch
        logDebug ari "events" $ show event
        if shouldHandle event
            then handler event
            else pure ()

allEvents :: EventFilter
allEvents = const True

ignoreEvents :: EventHandler
ignoreEvents = const (pure ())

-- TODO: invoke* that checks if events channel is up before sending
-- Perhaps a type family can be useful to indicate this "requirement"

invoke :: forall a.
    ( Typeable (ResponseOf a)
    , ToRequest a
    ) => ARI -> a -> IO (ResponseOf a)
invoke ari r = do
    logDebugShow ari "http" r
    let baseUrl = httpURL $ ariOptions ari
    let auth =
            HTTP.applyBasicAuth
                (BS.pack . fst . ariKey $ ariOptions ari)
                (BS.pack . snd . ariKey $ ariOptions ari)
    request <- fmap auth (toRequest baseUrl r)

    logDebug ari "http" $ BSL.fromChunks
        [ HTTP.method request
        , " "
        , HTTP.path request
        , HTTP.queryString request
        , "\n"
        , case HTTP.requestBody request of
            HTTP.RequestBodyLBS lbs -> BSL.toStrict lbs
            HTTP.RequestBodyBS bs   -> bs
            _                       -> "<streaming body>"
        ]
    response <- HTTP.httpLbs request (ariHTTP ari)

    let body = HTTP.responseBody response
    logDebug ari "http" body

    case cast () :: Maybe (ResponseOf a) of
        Just x  -> pure x
        Nothing -> either fail pure $
            eitherDecode' body

invokeStatus :: (Typeable (ResponseOf a), ToRequest a)
             => ARI -> a -> IO (Either Int (ResponseOf a))
invokeStatus ari r = do
    res <- try (invoke ari r)
    case res of
        Right x ->
            pure . Right $ x
        Left (HTTP.StatusCodeException s _ _) ->
            pure . Left $ HTTP.statusCode s
        Left e ->
            throwIO e

logDebug :: L.ToLogStr msg => ARI -> String -> msg -> IO ()
logDebug ari key msg =
    if key `elem` ariDebug (ariOptions ari)
        then do
            L.pushLogStrLn (ariLogger ari) $ L.toLogStr msg
            L.flushLogStr (ariLogger ari)
        else pure ()

logDebugShow :: Show a => ARI -> String -> a -> IO ()
logDebugShow ari key = logDebug ari key . show

logDebugText :: ARI -> String -> T.Text -> IO ()
logDebugText = logDebug
