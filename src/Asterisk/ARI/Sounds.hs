{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}

module Asterisk.ARI.Sounds
    ( List(..), list
    , Get(..)
    ) where

import           Asterisk.ARI.Transport.REST (ToRequest (..), toQuery)
import           Asterisk.ARI.Types.REST     (Sound (..))
import           Data.Text                   (Text, unpack, pack)

data List = List
    { lLang   :: !(Maybe Text)
    , lFormat :: !(Maybe Text)
    } deriving (Show)

instance ToRequest List where
    type ResponseOf List = [Sound]
    requestPath = const
        [ "sounds" ]
    requestQuery List{..} = toQuery
        [ fmap (pack "lang",)   lLang
        , fmap (pack "format",) lFormat
        ]

list :: List
list = List
    Nothing
    Nothing

data Get = Get !Text
    deriving (Show)

instance ToRequest Get where
    type ResponseOf Get = Sound
    requestPath (Get soundId) =
        [ "sounds", unpack soundId ]
