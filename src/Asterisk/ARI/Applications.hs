{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies    #-}

module Asterisk.ARI.Applications
    ( -- * Requests
      List(..)
    , Get(..)
    , Subscribe(..)
    , Unsubscribe(..)
    -- * Used response types
    , Application(..)
    ) where

import           Asterisk.ARI.Transport.REST (StdMethod (..), ToRequest (..),
                                              cs, urlParams)
import           Asterisk.ARI.Types.REST     (Application (..))
import           Data.Text                   (Text, unpack)

data List = List
    deriving (Show)

instance ToRequest List where
    type ResponseOf List = [Application]
    requestPath = const
        [ "applications" ]

data Get = Get !Text
    deriving (Show)

instance ToRequest Get where
    type ResponseOf Get = Application
    requestPath (Get appName) =
        [ "applications", unpack appName ]

data Subscribe = Subscribe
    { sAppName     :: !Text
    , sEventSource :: ![Text]
    } deriving (Show)

instance ToRequest Subscribe where
    type ResponseOf Subscribe = Application
    requestPath Subscribe{..} =
        [ "applications", unpack sAppName, "subscription" ]
    requestPayload Subscribe{..} = urlParams
        [ sEventSource `cs` "eventSource" ]

data Unsubscribe = Unsubscribe
    { uAppName     :: !Text
    , uEventSource :: ![Text]
    } deriving (Show)

instance ToRequest Unsubscribe where
    type ResponseOf Unsubscribe = Application
    requestMethod = const $ Just DELETE
    requestPath Unsubscribe{..} =
        [ "applications", unpack uAppName, "subscription" ]
    requestPayload Unsubscribe{..} = urlParams
        [ uEventSource `cs` "eventSource" ]
