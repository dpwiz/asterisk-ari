{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies    #-}

module Asterisk.ARI.Channels
    ( -- * Requests
      List(..)
    , Get(..)
    , Hangup(..)
    , Originate(..), originate, appOriginate
    -- * Used response types
    , Channel(..)
    , CallerID(..)
    , DialplanCEP(..)
    -- * Associated events
    , ChannelVarset(..)
    , ChannelStateChange(..)
    , ChannelHangupRequest(..)
    , ChannelDestroyed(..)
    , StasisStart(..)
    , StasisEnd(..)
    -- * Event processing helpers
    , eventChannel
    , forChannel
    ) where

import           Asterisk.ARI.Transport.REST (StdMethod (..), ToRequest (..),
                                              as, urlParams)
import           Asterisk.ARI.Types          (EventFilter)
import           Asterisk.ARI.Types.Events   (Event (..), FromEvent (..))
import           Asterisk.ARI.Types.REST     (CallerID (..), Channel (..),
                                              DialplanCEP (..))
import           Data.Aeson                  (FromJSON (..), withObject, (.:),
                                              (.:?))
import           Data.Aeson.Types            (parseMaybe)
import qualified Data.HashMap.Strict         as HM
import           Data.Text                   (Text, unpack)

data List = List
    deriving (Show)

instance ToRequest List where
    type ResponseOf List = [Channel]
    requestPath = const ["channels"]

data Get = Get !Text
    deriving (Show)

instance ToRequest Get where
    type ResponseOf Get = Channel
    requestPath (Get cid) = ["channels", unpack cid]

data Hangup = Hangup
    { hChannelId :: !Text
    , hReason    :: !Text
    } deriving (Show)

instance ToRequest Hangup where
    requestPath Hangup{..} = ["channels", unpack hChannelId]
    requestMethod = const $ Just DELETE

data Originate = Originate
    { oEndpoint       :: !Text
    , oExtension      :: !(Maybe Text)
    , oContext        :: !(Maybe Text)
    , oPriority       :: !(Maybe Integer)
    , oLabel          :: !(Maybe Text)
    , oApp            :: !(Maybe Text)
    , oAppArgs        :: !(Maybe Text)
    , oCallerId       :: !(Maybe Text)
    , oTimeout        :: !(Maybe Text)
    , oChannelId      :: !(Maybe Text)
    , oOtherChannelId :: !(Maybe Text)
    , oOriginator     :: !(Maybe Text)
    } deriving (Show)

instance ToRequest Originate where
    type ResponseOf Originate = Channel
    requestPath = const ["channels"]
    requestPayload Originate{..} = urlParams
        [ Just oEndpoint      `as` "endpoint"
        , oExtension          `as` "extension"
        , oContext            `as` "context"
        , fmap show oPriority `as` "priority"
        , oLabel              `as` "label"
        , oApp                `as` "app"
        , oAppArgs            `as` "appArgs"
        , oCallerId           `as` "callerId"
        , fmap show oTimeout  `as` "timeout"
        , oChannelId          `as` "channelId"
        , oOtherChannelId     `as` "otherChannelId"
        , oOriginator         `as` "originator"
        ]

originate :: Text -> Originate
originate endpoint = Originate
    endpoint
    Nothing
    Nothing
    Nothing
    Nothing
    Nothing
    Nothing
    Nothing
    Nothing
    Nothing
    Nothing
    Nothing

appOriginate :: Text -> Text -> Text -> Originate
appOriginate app appArgs ep = (originate ep)
    { oApp     = Just app
    , oAppArgs = Just appArgs
    }

eventChannel :: Event -> Maybe Channel
eventChannel e =
    HM.lookup "channel" (eventRaw e) >>=
        parseMaybe parseJSON

forChannel :: Text -> EventFilter
forChannel expected = maybe False ((==) expected . cId)
    . eventChannel

data ChannelVarset = ChannelVarset
    { cvsValue    :: !Text
    , cvsVariable :: !Text
    } deriving (Show)

instance FromJSON ChannelVarset where
    parseJSON = withObject "ChannelVarset" $ \o -> ChannelVarset
        <$> o .: "value"
        <*> o .: "variable"

instance FromEvent ChannelVarset

data ChannelStateChange = ChannelStateChange !Channel
    deriving (Show)

instance FromJSON ChannelStateChange where
    parseJSON = withObject "ChannelStateChange" $ \o -> ChannelStateChange
        <$> o .: "channel"

instance FromEvent ChannelStateChange

data ChannelHangupRequest = ChannelHangupRequest
    { chrCause :: !Int
    , chrSoft  :: !(Maybe Bool)
    } deriving (Show)

instance FromJSON ChannelHangupRequest where
    parseJSON = withObject "ChannelHangupRequest" $ \o -> ChannelHangupRequest
        <$> o .:  "cause"
        <*> o .:? "soft"

instance FromEvent ChannelHangupRequest

data ChannelDestroyed = ChannelDestroyed
    { cdCause    :: !Int
    , cdCauseTxt :: !Text
    } deriving (Show)

instance FromJSON ChannelDestroyed where
    parseJSON = withObject "ChannelDestroyed" $ \o -> ChannelDestroyed
        <$> o .: "cause"
        <*> o .: "cause_txt"

instance FromEvent ChannelDestroyed

data StasisStart = StasisStart !Channel
    deriving (Show)

instance FromJSON StasisStart where
    parseJSON = withObject "StasisStart" $ \o -> StasisStart
        <$> o .: "channel"

instance FromEvent StasisStart

data StasisEnd = StasisEnd !Channel
    deriving (Show)

instance FromJSON StasisEnd where
    parseJSON = withObject "StasisEnd" $ \o -> StasisEnd
        <$> o .: "channel"

instance FromEvent StasisEnd
