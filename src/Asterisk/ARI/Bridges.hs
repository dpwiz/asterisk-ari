{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies    #-}

module Asterisk.ARI.Bridges
    ( -- * Requests
      List(..)
    , Create(..), create
    , Get(..)
    , Destroy(..)
    , AddChannel(..), addChannel
    , RemoveChannel(..)
    , StartMoh(..), startMoh
    , StopMoh(..)
    , Play(..), play
    , Record(..), record
    -- * Used response types
    , Bridge(..)
    ) where

import           Asterisk.ARI.Transport.REST (StdMethod (..), ToRequest (..),
                                              as, cs, urlParams)
import           Asterisk.ARI.Types.REST     (Bridge (..))
import           Data.Text                   (Text, unpack)

data List = List
    deriving (Show)

instance ToRequest List where
    type ResponseOf List = [Bridge]
    requestPath = const
        [ "bridges" ]

data Create = Create
    { cType     :: ![Text]
    , cBridgeId :: !(Maybe Text)
    , cName     :: !(Maybe Text)
    } deriving (Show)

instance ToRequest Create where
    type ResponseOf Create = Bridge
    requestPath = const
        [ "bridges" ]
    requestPayload Create{..} = urlParams
        [ cType     `cs` "type"
        , cBridgeId `as` "bridgeId"
        , cName     `as` "name"
        ]

create :: Create
create = Create
    []
    Nothing
    Nothing

data Get = Get !Text
    deriving (Show)

instance ToRequest Get where
    type ResponseOf Get = Bridge
    requestPath (Get bridgeId) =
        [ "bridges", unpack bridgeId ]

data Destroy = Destroy !Text
    deriving (Show)

instance ToRequest Destroy where
    requestMethod = const $ Just DELETE
    requestPath (Destroy bridgeId) =
        [ "bridges", unpack bridgeId ]

data AddChannel = AddChannel
    { acBridgeId :: !Text
    , acChannel  :: ![Text]
    , acRole     :: !(Maybe Text)
    } deriving (Show)

instance ToRequest AddChannel where
    requestPath AddChannel{..} =
        [ "bridges", unpack acBridgeId, "addChannel" ]
    requestPayload AddChannel{..} = urlParams
        [ acChannel `cs` "channel"
        , acRole    `as` "role"
        ]

addChannel :: Text -> [Text] -> AddChannel
addChannel bid chs = AddChannel
    bid
    chs
    Nothing

data RemoveChannel = RemoveChannel
    { rcBridgeId :: !Text
    , rcChannel  :: ![Text]
    } deriving (Show)

instance ToRequest RemoveChannel where
    requestPath RemoveChannel{..} =
        [ "bridges", unpack rcBridgeId, "removeChannel" ]
    requestPayload RemoveChannel{..} = urlParams
        [ rcChannel `cs` "channel" ]

data StartMoh = StartMoh
    { smBridgeId :: !Text
    , smMohClass :: !(Maybe Text)
    } deriving (Show)

instance ToRequest StartMoh where
    requestPath StartMoh{..} =
        [ "bridges", unpack smBridgeId, "moh" ]
    requestPayload StartMoh{..} = urlParams
        [ smMohClass `as` "mohClass" ]

startMoh :: Text -> StartMoh
startMoh bid = StartMoh
    bid
    Nothing

data StopMoh = StopMoh !Text
    deriving (Show)

instance ToRequest StopMoh where
    requestPath (StopMoh bridgeId) =
        [ "bridges", unpack bridgeId, "moh" ]
    requestMethod = const $ Just DELETE

data Play = Play
    { pBridgeId   :: !Text
    , pMedia      :: !Text
    , pLang       :: !(Maybe Text)
    , pOffsetMs   :: !(Maybe Int)
    , pSkipMs     :: !(Maybe Int)
    , pPlaybackId :: !(Maybe Text)
    } deriving (Show)

instance ToRequest Play where
    requestPath Play{..} =
        [ "bridges", unpack pBridgeId, "play" ]
    requestPayload Play{..} = urlParams
        [ Just pMedia         `as` "media"
        , pLang               `as` "lang"
        , fmap show pOffsetMs `as` "offsetms"
        , fmap show pSkipMs   `as` "skipms"
        , pPlaybackId         `as` "playbackId"
        ]

play :: Text -> Text -> Play
play bid media = Play
    bid
    media
    Nothing
    Nothing
    Nothing
    Nothing

data Record = Record
    { rBridgeId           :: !Text
    , rName               :: !Text
    , rFormat             :: !Text
    , rMaxDurationSeconds :: !(Maybe Int)
    , rMaxSilenceSeconds  :: !(Maybe Int)
    , rIfExists           :: !(Maybe Text)
    , rBeep               :: !(Maybe Bool)
    , rTerminateOn        :: !(Maybe Text)
    } deriving (Show)

instance ToRequest Record where
    requestPath Record{..} =
        [ "bridges", unpack rBridgeId, "record" ]
    requestPayload Record{..} = urlParams
        [ Just rName                    `as` "name"
        , Just rFormat                  `as` "format"
        , fmap show rMaxDurationSeconds `as` "maxDurationSeconds"
        , fmap show rMaxSilenceSeconds  `as` "maxSilenceSeconds"
        , rIfExists                     `as` "ifExists"
        , fmap show rBeep               `as` "beep"
        , rTerminateOn                  `as` "terminateOn"
        ]

record :: Text -> Text -> Text -> Record
record bid name fmt = Record
    bid
    name
    fmt
    Nothing
    Nothing
    Nothing
    Nothing
    Nothing
