module Asterisk.ARI.Types
    ( Options(..), httpURL, wsURL
    , WSOptions(..)
    , ARI(..)
    , Event(..), EventsApp
    , EventFilter, EventHandler
    ) where

import           Control.Concurrent            (ThreadId)
import qualified Control.Concurrent.Chan.Unagi as CH
import           Control.Exception             (SomeException)
import           Data.IORef                    (IORef)
import qualified Network.HTTP.Client           as HTTP
import qualified System.Log.FastLogger         as L

import           Asterisk.ARI.Types.Events     (Event (..))

data Options = Options
    { ariApp    :: String
    , ariKey    :: (String, String)
    , ariSecure :: Bool
    , ariHost   :: String
    , ariPort   :: Int
    , ariPath   :: String
    , ariDebug  :: [String]
    }

data WSOptions = WSOptions
    { wsConnect   :: Bool
    , wsOnConnect :: ThreadId -> IO ()
    , wsFinally   :: Maybe SomeException -> IO Bool
    }

ariURL :: Bool -> Options -> String
ariURL ws o = concat
    [ if ws then "ws" else "http"
    , if ariSecure o then "s" else ""
    , "://"
    , ariHost o
    , ":"
    , show $ ariPort o
    , ariPath o
    ]

httpURL, wsURL :: Options -> String
httpURL = ariURL False
wsURL = ariURL True

data ARI = ARI
    { ariOptions   :: Options
    , ariLogger    :: L.LoggerSet
    , ariHTTP      :: HTTP.Manager
    , ariEventsApp :: EventsApp
    }

type EventsApp = (CH.InChan Event, IORef (Maybe ThreadId))

type EventFilter = Event -> Bool
type EventHandler = Event -> IO ()
