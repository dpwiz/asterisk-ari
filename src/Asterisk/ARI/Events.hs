{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies    #-}

module Asterisk.ARI.Events
    ( -- * Requests
      UserEvent(..), userEvent
      -- * Associated events
    , ChannelUserevent(..)
    ) where

import           Asterisk.ARI.Transport.REST
import           Asterisk.ARI.Types.Events   (FromEvent)
import           Asterisk.ARI.Types.REST     (Bridge, Channel, Endpoint)
import           Data.Aeson                  (FromJSON (..), Object, object,
                                              withObject, (.:), (.:?), (.=))
import qualified Data.HashMap.Strict         as HM
import           Data.Text                   (Text, intercalate, unpack)

data UserEvent = UserEvent
    { ueName        :: !Text
    , ueApplication :: !Text
    , ueSource      :: ![Text]
    , ueVariables   :: !Object
    } deriving (Show)

instance ToRequest UserEvent where
    requestPath UserEvent{..} =
        [ "events", "user", unpack ueName ]
    requestPayload UserEvent{..} =
        let
            kvs =
                if null ueSource
                    then main
                    else source : main

            source = "source" .= intercalate "," ueSource

            main =
                [ "application" .= ueApplication
                , "variables"   .= ueVariables
                ]
        in
            jsonParams (object kvs)

userEvent :: Text -> Text -> UserEvent
userEvent n a = UserEvent
    n
    a
    []
    HM.empty

data ChannelUserevent = ChannelUserevent
    { cuBridge    :: !(Maybe Bridge)
    , cuChannel   :: !(Maybe Channel)
    , cuEndpoint  :: !(Maybe Endpoint)
    , cuEventName :: !Text
    , cuUserEvent :: !Object
    } deriving (Show)

instance FromJSON ChannelUserevent where
    parseJSON = withObject "ChannelUserevent" $ \o -> ChannelUserevent
        <$> o .:? "bridge"
        <*> o .:? "channel"
        <*> o .:? "endpoint"
        <*> o .:  "eventname"
        <*> o .:  "userevent"

instance FromEvent ChannelUserevent
