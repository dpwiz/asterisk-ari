module Asterisk.ARI.Transport.WebSocket
    ( eventsApp, connectEvents
    ) where

import           Control.Concurrent            (ThreadId, forkFinally, forkIO,
                                                myThreadId)
import qualified Control.Concurrent.Chan.Unagi as CH
import           Control.Monad                 (forever, void)
import           Data.Aeson                    (eitherDecodeStrict')
import           Data.IORef                    (atomicWriteIORef, newIORef)
import qualified Network.WebSockets            as WS
import qualified Wuss                          as WSS

import           Asterisk.ARI.Types

connectEvents :: Options -> WSOptions -> IO EventsApp
connectEvents opts ws = do
    chan <- fmap fst CH.newChan
    ctid <- newIORef Nothing
    if wsConnect ws
        then void . forkIO $ restarting chan ctid
        else pure ()
    pure (chan, ctid)

  where
    path = mconcat
        [ ariPath opts
        , "events"
        , "?api_key="
        , fst (ariKey opts), ":", snd (ariKey opts)
        , "&app="
        , ariApp opts
        ]

    opener chan =
        if ariSecure opts
            then WSS.runSecureClient
                (ariHost opts)
                (fromIntegral $ ariPort opts)
                path
                (eventsApp chan $ wsOnConnect ws)
            else WS.runClient
                (ariHost opts)
                (ariPort opts)
                path
                (eventsApp chan $ wsOnConnect ws)

    restarting chan ctid = do
        tid <- forkFinally (opener chan) $ \res -> do
            atomicWriteIORef ctid Nothing
            shouldReconnect <- wsFinally ws $
                either Just (const Nothing) res
            if shouldReconnect
                then restarting chan ctid
                else pure ()

        atomicWriteIORef ctid (Just tid)

eventsApp :: CH.InChan Event -> (ThreadId -> IO ()) -> WS.ClientApp ()
eventsApp chan onConnect conn = do
    myThreadId >>= onConnect
    WS.forkPingThread conn 10
    forever $ do
        d <- WS.receiveData conn
        case eitherDecodeStrict' d of
            Left e -> fail $ "Event parsing failed: " ++ e
            Right v -> CH.writeChan chan v
