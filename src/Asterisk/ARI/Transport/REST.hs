{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies     #-}
module Asterisk.ARI.Transport.REST
    ( ToRequest(..)
    , Path
    , RequestPayload
    , HTTP.StdMethod(..)
    , HTTP.toQuery
    , urlParams, as, cs
    , jsonParams
    ) where

import           Data.Aeson            (FromJSON, ToJSON, encode)
import qualified Data.ByteString.Char8 as BS
import           Data.List             (intersperse)
import           Data.Maybe            (catMaybes)
import qualified Network.HTTP.Client   as HTTP
import qualified Network.HTTP.Types    as HTTP
import qualified Text.StringConvert    as SC

class (FromJSON (ResponseOf a), Show a) => ToRequest a where
    type ResponseOf a :: *
    type ResponseOf a = ()

    requestPath :: a -> Path

    requestQuery :: a -> HTTP.Query
    requestQuery = const []

    requestPayload :: a -> RequestPayload
    requestPayload = const Nothing

    -- | Override request method (to use DELETE, UPDATE, etc.)
    requestMethod :: a -> Maybe HTTP.StdMethod
    requestMethod = const Nothing

    toRequest :: String -> a -> IO HTTP.Request
    toRequest baseUrl r = do
        let url = concat
                $ baseUrl
                : intersperse "/" (requestPath r)

        let setQuery hr =
                case requestQuery r of
                    [] -> hr
                    ps -> hr
                        { HTTP.queryString =
                            HTTP.renderQuery True ps
                        }

        let setPayload hr =
                case requestPayload r of
                    Nothing         -> hr
                    Just (ct, body) -> hr
                        { HTTP.method = HTTP.methodPost
                        , HTTP.requestHeaders =
                            (HTTP.hContentType, ct) : HTTP.requestHeaders hr
                        , HTTP.requestBody = body
                        }

        let setMethod hr =
                case requestMethod r of
                    Nothing -> hr
                    Just sm -> hr
                        { HTTP.method = HTTP.renderStdMethod sm
                        }

        fmap (setMethod . setPayload . setQuery) (HTTP.parseUrl url)

type Path = [String]

type ItemKey = BS.ByteString
type ItemVal = BS.ByteString

as :: SC.StringConvert a ItemVal
    => Maybe a
    -> ItemKey
    -> Maybe HTTP.SimpleQueryItem
mval `as` key = case mval of
    Nothing -> Nothing
    Just x -> Just (key, SC.s x)

cs :: SC.StringConvert a ItemVal
    => [a]
    -> ItemKey
    -> Maybe HTTP.SimpleQueryItem
cs xs key =
    if null xs
        then Nothing
        else Just (key, val)
  where
    val = BS.intercalate "," (map SC.s xs)

type RequestPayload = Maybe (BS.ByteString, HTTP.RequestBody)

urlParams :: [Maybe HTTP.SimpleQueryItem] -> RequestPayload
urlParams ps = Just (ct, body)
  where
    ct = "application/x-www-form-urlencoded"
    body = HTTP.RequestBodyBS
        . HTTP.renderSimpleQuery False
        $ catMaybes ps

jsonParams :: ToJSON a => a -> RequestPayload
jsonParams val = Just (ct, body)
  where
    ct = "application/json"
    body = HTTP.RequestBodyLBS (encode val)
