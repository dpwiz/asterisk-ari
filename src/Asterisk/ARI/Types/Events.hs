{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE RecordWildCards           #-}
{-# LANGUAGE ScopedTypeVariables       #-}

module Asterisk.ARI.Types.Events
    ( Event(..)
    , FromEvent(..)
    , EventHandler(..), handles
    ) where

import           Data.Aeson                 (FromJSON (..), Object, Value (..),
                                             withObject, (.:))
import           Data.Aeson.Encode.Pretty   (encodePretty)
import           Data.Aeson.Types           (parseEither)
import qualified Data.ByteString.Lazy.Char8 as BSL
import qualified Data.HashMap.Strict        as HM
import           Data.Text                  (Text, pack, unpack)
import           Data.Typeable

data Event
    = Event
    { eventType :: !Text
    , eventApp  :: !Text
    , eventTS   :: !Text
    , eventRaw  :: !Object
    }

instance Show Event where
    show Event{..} = concat
        [ "Event {\n"
        , "    type = ", show eventType, "\n"
        , "    app = ", show eventApp, "\n"
        , "    ts = ", show eventTS, "\n"
        , "} # "
        , BSL.unpack $ encodePretty eventRaw
        ]

instance FromJSON Event where
    parseJSON = withObject "Event" $ \o -> Event
        <$> o .: "type"
        <*> o .: "application"
        <*> o .: "timestamp"
        <*> pure o

class (Typeable a, FromJSON a) => FromEvent a where
    fromEvent :: Event -> Either String a
    fromEvent e =
        if actual == expected
            then parseEither parseJSON (Object $ eventRaw e)
            else Left $ concat
                [ "Invalid type: "
                , actual
                , " (expected: "
                , expected
                , ")"
                ]

      where
        actual = unpack (eventType e)
        expected = typeName (Proxy :: Proxy a)
        typeName = tyConName . typeRepTyCon . typeRep

-- instance FromEvent Event where
--     fromEvent = Right

handles :: [EventHandler] -> (Event -> IO ()) -> Event -> IO ()
handles handlers sink =
    \someEvent -> case HM.lookup (eventType someEvent) typeMap of
        Nothing -> sink someEvent
        Just (EventHandler handle) -> case fromEvent someEvent of
            Right event -> handle event
            Left err    -> fail err
  where
    typeMap :: HandlerIndex
    typeMap = foldr register HM.empty handlers

    register :: EventHandler -> HandlerIndex -> HandlerIndex
    register h@(EventHandler (_ :: a -> IO ())) =
        let eventType = typeName (Proxy :: Proxy a)
            typeName = tyConName . typeRepTyCon . typeRep
        in HM.insert (pack eventType) h

data EventHandler = forall e. FromEvent e => EventHandler (e -> IO ())

type HandlerIndex = HM.HashMap Text EventHandler
