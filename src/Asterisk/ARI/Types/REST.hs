module Asterisk.ARI.Types.REST
    ( Endpoint(..)
    , Channel(..)
    , CallerID(..)
    , DialplanCEP(..)
    , FormatLangPair(..)
    , Sound(..)
    , Bridge(..)
    , Application(..)
    ) where

import           Data.Aeson
import           Data.Text  (Text)

data Endpoint = Endpoint
    { eChannelIds :: ![Text]
    , eResource   :: !Text
    , eState      :: !(Maybe Text)
    , eTechnology :: !Text
    } deriving (Show)

instance FromJSON Endpoint where
    parseJSON = withObject "Endpoint" $ \o -> Endpoint
        <$> o .:  "channel_ids"
        <*> o .:  "resource"
        <*> o .:? "state"
        <*> o .:  "technology"

data Channel = Channel
    { cAccountCode  :: !Text
    , cCaller       :: !CallerID
    , cConnected    :: !CallerID
    , cCreationTime :: !Text
    , cDialplan     :: !DialplanCEP
    , cId           :: !Text
    , cLanguage     :: !Text
    , cName         :: !Text
    , cState        :: !Text
    } deriving (Show)

instance FromJSON Channel where
    parseJSON = withObject "Channel" $ \o -> Channel
        <$> o .: "accountcode"
        <*> o .: "caller"
        <*> o .: "connected"
        <*> o .: "creationtime"
        <*> o .: "dialplan"
        <*> o .: "id"
        <*> o .: "language"
        <*> o .: "name"
        <*> o .: "state"

data CallerID = CallerID
    { cidName   :: !Text
    , cidNumber :: !Text
    } deriving (Show)

instance FromJSON CallerID where
    parseJSON = withObject "CallerID" $ \o -> CallerID
        <$> o .: "name"
        <*> o .: "number"

data DialplanCEP = DialplanCEP
    { cepContext  :: !Text
    , cepExten    :: !Text
    , cepPriority :: !Integer
    } deriving (Show)

data Bridge = Bridge
    { bClass      :: !Text
    , bType       :: !Text
    , bChannels   :: ![Text]
    , bCreator    :: !Text
    , bId         :: !Text
    , bName       :: !Text
    , bTechnology :: !Text
    } deriving (Show)

instance FromJSON Bridge where
    parseJSON = withObject "Bridge" $ \o -> Bridge
        <$> o .: "bridge_class"
        <*> o .: "bridge_type"
        <*> o .: "channels"
        <*> o .: "creator"
        <*> o .: "id"
        <*> o .: "name"
        <*> o .: "technology"

instance FromJSON DialplanCEP where
    parseJSON = withObject "DialplanCEP" $ \o -> DialplanCEP
        <$> o .: "context"
        <*> o .: "exten"
        <*> o .: "priority"

data FormatLangPair = FormatLangPair
    { flpFormat   :: !Text
    , flpLanguage :: !Text
    } deriving (Show)

instance FromJSON FormatLangPair where
    parseJSON = withObject "FormatLangPair" $ \o -> FormatLangPair
        <$> o .: "format"
        <*> o .: "language"

data Sound = Sound
    { sFormats :: ![FormatLangPair]
    , sId      :: !Text
    , sText    :: !(Maybe Text)
    } deriving (Show)

instance FromJSON Sound where
    parseJSON = withObject "Sound" $ \o -> Sound
        <$> o .:  "formats"
        <*> o .:  "id"
        <*> o .:? "text"

data Application = Application
    { aBridgeIds   :: ![Text]
    , aChannelIds  :: ![Text]
    , aDeviceNames :: ![Text]
    , aEndpointIds :: ![Text]
    , aName        :: !Text
    } deriving (Show)

instance FromJSON Application where
    parseJSON = withObject "Application" $ \o -> Application
        <$> o .: "bridge_ids"
        <*> o .: "channel_ids"
        <*> o .: "device_names"
        <*> o .: "endpoint_ids"
        <*> o .: "name"
