{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Asterisk.ARI.Monad
    ( HasARI(..), ARI
    , getARIappName
    , withEventHandler
    , invokeARI
    , invokeARIstatus
    , logDebugARI
    , WithARI, withARI, runARI
    , ariToIO
    , MonadBaseControl, StM
    , MonadIO, liftIO
    ) where

import           Control.Concurrent.Lifted   (fork, killThread)
import           Control.Monad.Catch         (MonadMask, finally)
import           Control.Monad.Reader        (ReaderT, runReaderT, ask)
import           Control.Monad.Trans         (MonadIO, liftIO)
import           Control.Monad.Trans.Control (MonadBaseControl, StM)
import           Data.Text                   (Text, pack)
import           Data.Typeable               (Typeable)
import qualified System.Log.FastLogger       as L

import           Asterisk.ARI
import           Asterisk.ARI.Transport.REST

-- * ARI-containg class

class Monad m => HasARI m where
    getARI :: m ARI

-- * Monadic wrappers

getARIappName :: HasARI m => m Text
getARIappName = fmap
    (pack . ariApp . ariOptions)
    getARI

-- | Beware! Action must not fork away
-- or the event handler goes down.
withEventHandler ::
    ( HasARI m
    , MonadMask m
    , MonadBaseControl IO m
    , MonadIO m
    )
    => (Event -> Bool)
    -> (Event -> IO ())
    -> m a
    -> m a
withEventHandler sh h m = do
    ari <- getARI
    tid <- fork . liftIO $ handleEvents ari sh h
    m `finally` killThread tid

invokeARI ::
    ( ToRequest a
    , Typeable (ResponseOf a)
    , HasARI m
    , MonadIO m
    ) => a -> m (ResponseOf a)
invokeARI req = do
    ari <- getARI
    liftIO $ invoke ari req

invokeARIstatus ::
    ( ToRequest a
    , Typeable (ResponseOf a)
    , HasARI m
    , MonadIO m
    ) => a -> m (Either Int (ResponseOf a))
invokeARIstatus req = do
    ari <- getARI
    liftIO $ invokeStatus ari req

logDebugARI ::
    ( L.ToLogStr msg
    , HasARI m
    , MonadIO m
    ) => String -> msg -> m ()
logDebugARI key msg = do
    ari <- getARI
    liftIO $ logDebug ari key msg

-- * Basic Reader wrapper on IO

type WithARI a = ReaderT ARI IO a

runARI :: WithARI a -> ARI -> IO a
runARI = runReaderT

withARI :: ARI -> WithARI a -> IO a
withARI = flip runARI

ariToIO :: ((WithARI a -> IO a) -> IO b) -> WithARI b
ariToIO ioAction = do
    ari <- getARI
    liftIO $ ioAction (withARI ari)

instance HasARI (ReaderT ARI IO) where
    getARI = ask
