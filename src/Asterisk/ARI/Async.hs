{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}

module Asterisk.ARI.Async
    ( withAsyncHandler
    , eventFlag
    , onFlag
    , onFlag_
    , waitFlag
    , Async
    , MVar
    ) where

import qualified Asterisk.ARI                    as ARI
import           Asterisk.ARI.Monad              (HasARI, getARI, liftIO)
import           Asterisk.ARI.Types.Events       (EventHandler (..), FromEvent)
import           Control.Concurrent.Async.Lifted (Async, async)
import           Control.Concurrent.MVar.Lifted  (MVar, newEmptyMVar, putMVar, readMVar)
import           Control.Monad.Trans             (MonadIO)
import           Control.Monad.Trans.Control     (MonadBaseControl, StM)

withAsyncHandler ::
    ( HasARI m
    , MonadBaseControl IO m
    , MonadIO m
    )
    => (ARI.Event -> Bool)
    -> (ARI.Event -> IO ())
    -> m a
    -> m (Async (StM m ()), a)
withAsyncHandler f h m = do
    ari <- getARI
    cancelMe <- async . liftIO $ ARI.handleEvents ari f h
    (,) <$> pure cancelMe <*> m

eventFlag ::
    ( FromEvent a
    , MonadBaseControl IO m
    ) => m (MVar a, EventHandler)
eventFlag = do
    flag <- newEmptyMVar
    pure (flag, EventHandler $ putMVar flag)

onFlag :: MonadBaseControl IO m
    => MVar a
    -> (a -> m b)
    -> m (Async (StM m b))
onFlag flag action = async $
    readMVar flag >>= action

onFlag_ :: MonadBaseControl IO m
    => MVar a
    -> m b
    -> m ()
onFlag_ flag action = onFlag flag
    (const action) >> pure ()

waitFlag :: MonadBaseControl IO m
    => MVar a
    -> m (Async (StM m ()))
waitFlag flag = onFlag flag
    (const $ pure ())
