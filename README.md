This version implements [Asterisk 13 ARI](ARI-13).

[ARI-13]: https://wiki.asterisk.org/wiki/display/AST/Asterisk+13+ARI

## Example

``` haskell
import qualified Asterisk.ARI          as ARI
import qualified Asterisk.ARI.Channels as Channels
import           Control.Concurrent

main :: IO ()
main = ARI.envOptions
    >>= ARI.connect
    >>= ARI.waitReady
    >>= either (fail . show) somethingUseful

somethingUseful :: ARI.ARI -> IO ()
somethingUseful ari = do
    killMe <- forkIO $ ARI.handleEvents ari (const True) print

    print =<< ARI.invoke ari
        (Channels.originate "SIP/1234567890@somewhere")
            { Channels.oLabel    = Just "test"
            , Channels.oPriority = Just 42
            }

    threadDelay 1000000 -- do something useful
    killThread killMe -- don't forget to clean up
```

## Implementation progress

### REST endpoints

  * [x] Applications
     * [x] list
     * [x] get
     * [x] subscribe
     * [x] unsubscribe
  * [ ] Asterisk
     * [ ] getObject
     * [ ] updateObject
     * [ ] deleteObject
     * [ ] getInfo
     * [ ] listModules
     * [ ] getModule
     * [ ] loadModule
     * [ ] unloadModule
     * [ ] reloadModule
     * [ ] listLogChannels
     * [ ] addLog
     * [ ] deleteLog
     * [ ] rotateLog
     * [ ] getGlobalVar
     * [ ] setGlobalVar
  * [x] Bridges
     * [x] list
     * [x] create
     * [x] get
     * [x] destroy
     * [x] addChannel
     * [x] removeChannel
     * [x] startMoh
     * [x] stopMoh
     * [x] play
     * [x] record
  * [ ] Channels
     * [x] list
     * [x] originate
     * [ ] get
     * [ ] originateWithId
     * [x] hangup
     * [ ] continueInDialplan
     * [ ] redirect
     * [ ] answer
     * [ ] ring
     * [ ] ringStop
     * [ ] sendDTMF
     * [ ] mute
     * [ ] unmute
     * [ ] hold
     * [ ] unhold
     * [ ] startMoh
     * [ ] stopMoh
     * [ ] startSilence
     * [ ] stopSilence
     * [ ] play
     * [ ] playWithId
     * [ ] record
     * [ ] getChannelVar
     * [ ] setChannelVar
     * [ ] snoopChannel
     * [ ] snoopChannelWithId
  * [ ] Devicestates
     * [ ] list
     * [ ] get
     * [ ] update
     * [ ] delete
  * [ ] Endpoints
     * [ ] list
     * [ ] sendMessage
     * [ ] listByTech
     * [ ] get
     * [ ] sendMessageToEndpoint
  * [ ] Events
     * [x] eventWebsocket
     * [x] userEvent
  * [ ] Mailboxes
     * [ ] list
     * [ ] get
     * [ ] update
     * [ ] delete
  * [ ] Playbacks
     * [ ] get
     * [ ] stop
     * [ ] control
  * [ ] Recordings
     * [ ] listStored
     * [ ] getStored
     * [ ] deleteStored
     * [ ] copyStored
     * [ ] getLive
     * [ ] cancel
     * [ ] stop
     * [ ] pause
     * [ ] unpause
     * [ ] mute
     * [ ] unmute
  * [x] Sounds
     * [x] list
     * [x] get

### Data models

  * [ ] AsteriskInfo
  * [ ] BuildInfo
  * [ ] ConfigInfo
  * [ ] ConfigTuple
  * [ ] LogChannel
  * [ ] Module
  * [ ] SetId
  * [ ] StatusInfo
  * [ ] SystemInfo
  * [ ] Variable
  * [x] Endpoint
  * [ ] TextMessage
  * [ ] TextMessageVariable
  * [x] CallerID
  * [x] Channel
  * [ ] Dialed
  * [x] DialplanCEP
  * [x] Bridge
  * [ ] LiveRecording
  * [ ] StoredRecording
  * [x] FormatLangPair
  * [x] Sound
  * [ ] Playback
  * [ ] DeviceState
  * [ ] Mailbox
  * [ ] ApplicationReplaced
  * [ ] BridgeAttendedTransfer
  * [ ] BridgeBlindTransfer
  * [ ] BridgeCreated
  * [ ] BridgeDestroyed
  * [ ] BridgeMerged
  * [ ] ChannelCallerId
  * [ ] ChannelConnectedLine
  * [ ] ChannelCreated
  * [x] ChannelDestroyed
  * [ ] ChannelDialplan
  * [ ] ChannelDtmfReceived
  * [ ] ChannelEnteredBridge
  * [x] ChannelHangupRequest
  * [ ] ChannelHold
  * [ ] ChannelLeftBridge
  * [x] ChannelStateChange
  * [ ] ChannelTalkingFinished
  * [ ] ChannelTalkingStarted
  * [ ] ChannelUnhold
  * [x] ChannelUserevent
  * [x] ChannelVarset
  * [ ] ContactInfo
  * [ ] ContactStatusChange
  * [ ] DeviceStateChanged
  * [ ] Dial
  * [ ] EndpointStateChange
  * [ ] Event
  * [ ] Message
  * [ ] MissingParams
  * [ ] Peer
  * [ ] PeerStatusChange
  * [ ] PlaybackFinished
  * [ ] PlaybackStarted
  * [ ] RecordingFailed
  * [ ] RecordingFinished
  * [ ] RecordingStarted
  * [x] StasisEnd
  * [x] StasisStart
  * [ ] TextMessageReceived
  * [x] Application
